package com.example.talkativememe;

import java.io.File;

import com.example.talkativememe.db.MemesDAO;
import com.example.talkativememe.db.MySQLiteHelper;
import com.example.talkativememe.model.MemeModel;
import com.example.talkativememe.service.VozService;
import com.example.talkativememe.tools.Tools;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.widget.Toast;

public class TalkativeMeme extends Application implements OnSharedPreferenceChangeListener {

	private String mAppFilesPath = null;
	
	SharedPreferences sharedPrefs;
	
	String strBackgroundImagePath;
	
	// Singleton
	private static TalkativeMeme singleton;
	public static TalkativeMeme getInstance() {
		return singleton;
	}
	public static MemesDAO dao;
	private static Context context;
	
	private static final int TEXT_MEME_MAX_LENGTH = 50;
	private static final int MAX_LENGTH_AUDIO_SECS = 15;
	private static final int MAX_LENGTH_AUDIO_MS = MAX_LENGTH_AUDIO_SECS * 1000;
	
	private AudioRecordPlay audioRecordPlayManager;
	
	public String getAppFilesPath() {
		return mAppFilesPath;
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences arg0, String arg1) {
		// TODO Auto-generated method stub
		
	}
	
	public void AddNewMeme(long memeType, String actualMeme,
			String userEditedMeme) {
		dao.createMeme(memeType, actualMeme, userEditedMeme);
	}
	
	public int deleteMeme(long memeId) {
		int rowsDeleted = 0;

		rowsDeleted = dao.deleteMeme(memeId);

		return rowsDeleted;
	}

	
	public int deleteAllMemes() {
		int rowsDeleted = 0;
		rowsDeleted = dao.deleteAllMemes();
		return rowsDeleted;
	}

	public Cursor getAllMemes() {
		Cursor cursor = dao.getAllMemes();
		return cursor;
	}

	public Cursor getEntriesByName(String inputText) {
		Cursor cursor = dao.getMemesByName(inputText);
		return cursor;
	}

	public MemeModel getRandomEntry() {
		MemeModel entry = dao.getRandomMeme();
		return entry;
	}
	
	public int updateText(long memeId, long memeType,String newText) {
		int rowsAffected = 0;

		rowsAffected = dao.updateText(memeId, memeType, newText);


		return rowsAffected;
	}
	
	public String getFileNamesPrefix() {
		return null;
	}
	
	
	public int getMaxLengthAudioSeconds() {
		return MAX_LENGTH_AUDIO_SECS;
	}


	public int getMaxLengthTextEntry() {
		return TEXT_MEME_MAX_LENGTH;
	}
	
	public int getMaxLengthAudioMs() {
		return MAX_LENGTH_AUDIO_MS;
	}
	
	Bitmap[] m_bitmapDice;

	Bitmap m_bitmapLector;
	
	String strAnimDice0_Path, strAnimDice1_Path, strAnimDice2_Path;
	
	private static final int POSITION_ANIMATION_DICE_0 = 0;
	private static final int POSITION_ANIMATION_DICE_1 = 1;
	private static final int POSITION_ANIMATION_DICE_2 = 2;
	private static final int POSITION_LECTOR_BACKGROUND = 3;
	
	public int getPositionPapiroBackground() {
		return POSITION_LECTOR_BACKGROUND;
	}

	public int getPositionAnimationPhase0() {
		return POSITION_ANIMATION_DICE_0;
	}

	public int getPositionAnimationPhase1() {
		return POSITION_ANIMATION_DICE_1;
	}

	public int getPositionAnimationPhase2() {
		return POSITION_ANIMATION_DICE_2;
	}

	private String getAnimationDice0_Path() {
		return strAnimDice0_Path;
	}
	

	private String getAnimationDice1_Path() {
		return strAnimDice1_Path;
	}

	private String getAnimationDice2_Path() {
		return strAnimDice2_Path;
	}
	
	private String getBackgroundImagePath() {
		return strBackgroundImagePath;
	}
	
	public Bitmap getBackgroundBitmap(int position) {
		if (position == POSITION_LECTOR_BACKGROUND)
			return m_bitmapLector;
		else
			return getBitmapDice(position);
	}
	
	private Bitmap getBitmapDice(int dice) {
		return m_bitmapDice[dice];
	}

	public Bitmap getBitmapLector() {
		return m_bitmapLector;
	}
	
	private boolean bDefaultLectorLoaded = false;
	
	public boolean isDefaultLectorLoaded() {
		return bDefaultLectorLoaded;
	}
	
	public String getMainActivityTitle() {
		String fallbackValue = getResources().getString(R.string.app_name);
		String result;

		String titleInPreferences = PreferenceManager.getDefaultSharedPreferences(
				getApplicationContext()).getString("main_activity_title",
				"NONE");
		titleInPreferences = titleInPreferences.trim();

		if(("NONE".equals(titleInPreferences)) ||
				("".equals(titleInPreferences))) {
			result = fallbackValue;
		}
		else
			result = titleInPreferences;

		return result;

	}
	
	public boolean isSdCardMounted(boolean isApplicationStart) {
		boolean result = (Environment.getExternalStorageState()
				.equals(Environment.MEDIA_MOUNTED));

		if (!result) {
			String stringToShow;

			if(isApplicationStart) stringToShow = context.getResources().getString(
					R.string.not_sdcard_on_start);
			else stringToShow = context.getResources().getString(
					R.string.not_sdcard);

			Toast.makeText(context, stringToShow, Toast.LENGTH_LONG).show();
		}

		return result;
	}
	
	public void playSelection(MemeModel selectedMeme) {
		String selectedString = selectedMeme.getActualMeme();

		// Log.d(TAG,"playSelection:" + selectedString);

		if (selectedMeme.getMemeType() == MySQLiteHelper.MEME_TYPE_AUDIO)
			audioRecordPlayManager.startPlaying(getAppFilesPath() + File.separator + selectedString);
		else {
			// Create an Intent for starting the LoggingService
			Intent startTTSServiceIntent = new Intent(this,
					VozService.class);

			// Put Logging message in intent
			startTTSServiceIntent.putExtra(VozService.MEME_TO_SAY, selectedString);

			// Start the Service
			// NOTE: This is a non-blocking call
			startService(startTTSServiceIntent);

		}
	}
	public AudioRecordPlay getAudioRecordPlayManager() {
		return audioRecordPlayManager;
	}

	@Override
	public void onCreate() {
		super.onCreate();

		// DB
		dao = new MemesDAO(this);
		dao.open();

		context = getApplicationContext();

		// NOTE: It is very important to write on the ExternalFilesDir, so that when the app
		// will be uninstalled, the files it created will be automatically removed.
		//
		// At first, though, i used the path returned by getExternalCacheDir which is dedicated
		// to cache files.
		// mAppFilesPath = context.getExternalCacheDir().getAbsolutePath();
		if(isSdCardMounted(true)) {
		mAppFilesPath = context
				.getExternalFilesDir(Environment.DIRECTORY_MUSIC)
				.getAbsolutePath();
		}
		else mAppFilesPath = "";

		// Singleton initialization
		singleton = this;

		// Shared Preferences
		sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);

		String mainActivityTitle = sharedPrefs.getString("main_activity_title",
				"NONE");
		if (mainActivityTitle.equals("NONE")) {
			Editor editor = sharedPrefs.edit();
			editor.putString("main_activity_title",
					getResources().getString(R.string.app_name));
			editor.commit();
		}

		sharedPrefs.registerOnSharedPreferenceChangeListener(this);

		strBackgroundImagePath = sharedPrefs.getString("backgroundImagePath",
				"");

		strAnimDice0_Path = sharedPrefs.getString("animationPhase0", "");
		strAnimDice1_Path = sharedPrefs.getString("animationPhase1", "");
		strAnimDice2_Path = sharedPrefs.getString("animationPhase2", "");

		m_bitmapLector = resolveRightBitmap(POSITION_LECTOR_BACKGROUND);
		m_bitmapDice = new Bitmap[3];
		m_bitmapDice[0] = resolveRightBitmap(POSITION_ANIMATION_DICE_0);
		m_bitmapDice[1] = resolveRightBitmap(POSITION_ANIMATION_DICE_1);
		m_bitmapDice[2] = resolveRightBitmap(POSITION_ANIMATION_DICE_2);



		audioRecordPlayManager = new AudioRecordPlay(context);

		/*
		 * Moved to a service
		 * Intent startTTSServiceIntent = new Intent(this,
		 * TextToSpeechService.class);
		 * 
		 * // Put Logging message in intent
		 * startTTSServiceIntent.putExtra(TextToSpeechService.QUOTE_TO_PLAY,
		 * "");
		 * 
		 * // Start the Service startService(startTTSServiceIntent);
		 */
	}
	
	private final int TARGET_DISPLAY_WIDTH = 600;
	private final int TARGET_DISPLAY_HEIGHT = 400;
	
	private Bitmap resolveRightBitmap(int position) {
		// decode the placeholder image
		String pathAnim = "";
		Bitmap resultBitmap;
		int fallbackResourceId = 0;

		switch (position) {
		case POSITION_LECTOR_BACKGROUND:
			// Sfondo del PapiroFragment
			pathAnim = getBackgroundImagePath();
			fallbackResourceId = R.drawable.play_fondo;

			// In PapiroActivity i need to know if the user has chosen a custom background or not,
			// in order to decide whether to show the quote in a TextView or not.
			bDefaultLectorLoaded = ("".equals(pathAnim));

			break;
		case POSITION_ANIMATION_DICE_0:
			// First animation frame
			pathAnim = getAnimationDice0_Path();
			fallbackResourceId = R.drawable.dice0;
			break;
		case POSITION_ANIMATION_DICE_1:
			// Second animation frame
			pathAnim = getAnimationDice1_Path();
			fallbackResourceId = R.drawable.dice1;
			break;
		case POSITION_ANIMATION_DICE_2:
			// Third animation frame
			pathAnim = getAnimationDice2_Path();
			fallbackResourceId = R.drawable.dice2;
			break;
		}

		if ("".equals(pathAnim)) {
			resultBitmap = decodeSampledBitmapFromResource(getResources(),
					fallbackResourceId, TARGET_DISPLAY_WIDTH,
					TARGET_DISPLAY_HEIGHT);
		} else {
			Uri uri = Uri.parse(pathAnim);

			String path = getRealPathFromURI(uri);

			if (path == null) {
				resultBitmap = decodeSampledBitmapFromResource(getResources(),
						fallbackResourceId, TARGET_DISPLAY_WIDTH,
						TARGET_DISPLAY_HEIGHT);
			} else {
				resultBitmap = decodeSampledBitmapFromFile(path,
						TARGET_DISPLAY_WIDTH, TARGET_DISPLAY_HEIGHT);
				if (resultBitmap == null) {
					resultBitmap = decodeSampledBitmapFromResource(
							getResources(), fallbackResourceId,
							TARGET_DISPLAY_WIDTH, TARGET_DISPLAY_HEIGHT);
				}
			}
		}

		return resultBitmap;
	}
	
	public String getRealPathFromURI(Uri contentUri) {
		return Tools.getPath(this, contentUri);
	}
	
	public Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
			int reqWidth, int reqHeight) {

		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeResource(res, resId, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth,
				reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeResource(res, resId, options);
	}
	
	public int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			// Calculate the largest inSampleSize value that is a power of 2 and
			// keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > reqHeight
					&& (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}

		return inSampleSize;
	}
	
	public Bitmap decodeSampledBitmapFromFile(String path, int reqWidth,
			int reqHeight) {

		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;

		BitmapFactory.decodeFile(path, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth,
				reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeFile(path, options);
	}
	
}
