package com.example.talkativememe.ui;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.example.talkativememe.R;
import com.example.talkativememe.TalkativeMeme;
import com.example.talkativememe.db.MySQLiteHelper;
import com.example.talkativememe.model.MemeModel;

public class LectorActivity extends SherlockActivity {
	
	int count = 0;
	MemeModel currentMeme;
	String stringToPlay;
	ImageView backgroundImage;
	TextView massimaSceltaTextView;
	private boolean mDoShowAndPlayQuote;

	Bitmap currentlyActiveBitmap;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lector_activity);

		backgroundImage = (ImageView) findViewById(R.id.lectorImageView);
		backgroundImage.setImageBitmap(TalkativeMeme.getInstance().getBitmapLector());
		mDoShowAndPlayQuote = true;
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		currentlyActiveBitmap = null;
	}

	@Override
	protected void onStart() {
		super.onStart();

		currentMeme = (MemeModel) getIntent().getParcelableExtra(
				DiceFragment.MEME_TO_PLAY);
	}

	@Override
	public void onPause() {
		super.onPause();
		TalkativeMeme.getInstance().getAudioRecordPlayManager().stopPlaying();
	}

	@Override
	public void onResume() {
		super.onResume();

		setTitle(TalkativeMeme.getInstance().getMainActivityTitle());

		massimaSceltaTextView = (TextView) findViewById(R.id.lectorTextView);
		String stringaTextView = currentMeme.getUserEditedMeme();

		if (mDoShowAndPlayQuote) {
			LayoutInflater inflater = getLayoutInflater();
			View layout = inflater.inflate(R.layout.lector_toast,
					(ViewGroup) findViewById(R.id.toast_layout_root));

			ImageView image = (ImageView) layout
					.findViewById(R.id.lector_toast_image);
			image.setImageResource(R.drawable.ic_small);
			TextView text = (TextView) layout
					.findViewById(R.id.lector_toast_text);
			text.setText(stringaTextView);

			Toast toast = new Toast(this);
			toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
			toast.setDuration(Toast.LENGTH_SHORT);
			toast.setView(layout);
			toast.show();

			// Playback of the quote
			if (currentMeme.getMemeType() == MySQLiteHelper.MEME_TYPE_TEXT) {
				TalkativeMeme.getInstance().playSelection(currentMeme);
			} else if (currentMeme.getMemeType() == MySQLiteHelper.MEME_TYPE_AUDIO) {
				// No playback if the sd card is not mounted
				if (TalkativeMeme.getInstance().isSdCardMounted(false)) {
					TalkativeMeme.getInstance().playSelection(currentMeme);
				}
			}

			mDoShowAndPlayQuote = false;
		}

		if (TalkativeMeme.getInstance().isDefaultLectorLoaded()) {
			massimaSceltaTextView.setText(stringaTextView);
			massimaSceltaTextView.setVisibility(View.VISIBLE);
		} else
			massimaSceltaTextView.setVisibility(View.INVISIBLE);

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    // Respond to the action bar's Up/Home button
	    case android.R.id.home:
	        NavUtils.navigateUpFromSameTask(this);
	        return true;
	    }
	    return super.onOptionsItemSelected(item);
	}

}
