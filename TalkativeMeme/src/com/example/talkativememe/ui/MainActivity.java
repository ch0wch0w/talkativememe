package com.example.talkativememe.ui;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.example.talkativememe.R;
import com.example.talkativememe.adapters.LateralMenuListAdapter;
import com.example.talkativememe.model.LateralMenuItem;





public class MainActivity extends SherlockFragmentActivity  {

	private DrawerLayout mLateralLayout;
	private ListView mLateralList;
	private ActionBarDrawerToggle mLateralToggle;
	
	private CharSequence mLateralValue;
	private CharSequence appName;
	
	private String[] mMenuValues;
	private TypedArray mMenuIcons;
	private ArrayList<LateralMenuItem> lateralMenuItems;
	private LateralMenuListAdapter adapter;
	
	private final int IDX_LATERALMENU_HOME = 0;
	private final int IDX_LATERALMENU_ADD_MEME = 1;
	private final int IDX_LATERALMENU_MANAGE_MEMES = 2;
	private final int IDX_LATERALMENU_SETTINGS = 3;
	private final int IDX_LATERALMENU_HELP = 4;
	
	SharedPreferences sharedPrefs;
	boolean bFirstHelpScreen;
	int iAppVersion;
	
	boolean m_ShowAddNewMemeAction;
	boolean m_ShowDeleteAllMemes;
	
	ManageMemesFragment manageMemesFragment;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		appName = this.getTitle();
		mLateralValue = this.getTitle();
		
		mMenuValues = getResources().getStringArray(R.array.lateral_menu_items);
		mMenuIcons = getResources().obtainTypedArray(R.array.lateral_menu_icons);
		
		mLateralLayout = (DrawerLayout) findViewById(R.id.lateral_layout);
		mLateralList = (ListView) findViewById(R.id.list_lateralmenu);
		
		lateralMenuItems = new ArrayList<LateralMenuItem>();
		
		lateralMenuItems.add(new LateralMenuItem(mMenuValues[IDX_LATERALMENU_HOME],
				mMenuIcons.getResourceId(IDX_LATERALMENU_HOME, -1)));
		
		lateralMenuItems.add(new LateralMenuItem(mMenuValues[IDX_LATERALMENU_ADD_MEME],
				mMenuIcons.getResourceId(IDX_LATERALMENU_ADD_MEME, -1)));
		
		lateralMenuItems.add(new LateralMenuItem(mMenuValues[IDX_LATERALMENU_MANAGE_MEMES],
				mMenuIcons.getResourceId(IDX_LATERALMENU_MANAGE_MEMES, -1)));
		
		lateralMenuItems.add(new LateralMenuItem(mMenuValues[IDX_LATERALMENU_SETTINGS],
				mMenuIcons.getResourceId(IDX_LATERALMENU_SETTINGS, -1)));
		
		lateralMenuItems.add(new LateralMenuItem(mMenuValues[IDX_LATERALMENU_HELP],
				mMenuIcons.getResourceId(IDX_LATERALMENU_HELP, -1)));
		
		mMenuIcons.recycle();
		
		mLateralList.setOnItemClickListener(new SlideMenuClickListener());
		
		adapter = new LateralMenuListAdapter(getApplicationContext(), lateralMenuItems);
		mLateralList.setAdapter(adapter);
		
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		
		mLateralToggle = new ActionBarDrawerToggle(this, mLateralLayout, 
				R.drawable.ic_drawer, //icono
				R.string.app_name, //abierto
				R.string.app_name //cerrado
				) {
			
			public void onDrawerClosed(View view) {
				getSupportActionBar().setTitle(mLateralValue);

				supportInvalidateOptionsMenu();
			}
			
			public void onDrawerOpened(View drawerView) {
				InputMethodManager inputMethodManager = 
						(InputMethodManager) MainActivity.this.getSystemService(Activity.INPUT_METHOD_SERVICE);
				inputMethodManager.hideSoftInputFromWindow(
						MainActivity.this.getCurrentFocus().getWindowToken(), 0);

				getSupportActionBar().setTitle(mLateralValue);

				supportInvalidateOptionsMenu();
			}
		};
		mLateralLayout.setDrawerListener(mLateralToggle);
		
		// FIRST START ?
		if (savedInstanceState == null) {
			sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);

			bFirstHelpScreen = sharedPrefs.getBoolean("firstHelpScreen", true);

			iAppVersion = sharedPrefs.getInt("appVersion", 0);

			// ?
			if (bFirstHelpScreen || iAppVersion != 1) {
				displayView(IDX_LATERALMENU_HELP);

				Editor editor = sharedPrefs.edit();
				editor.putBoolean("firstHelpScreen", false);
				editor.putInt("appVersion", 1);
				editor.commit();
			}
			else 
				displayView(IDX_LATERALMENU_HELP);
		}

		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		new MenuInflater(this).inflate(R.menu.main, menu);	
		return (super.onCreateOptionsMenu(menu));
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {

			if (mLateralLayout.isDrawerOpen(mLateralList)) {
				mLateralLayout.closeDrawer(mLateralList);
			} else {
				mLateralLayout.openDrawer(mLateralList);
			}
		}
		int id = item.getItemId();
		
		if(id== R.id.action_new){
			displayView(IDX_LATERALMENU_ADD_MEME);
			supportInvalidateOptionsMenu();
		} else if(id== R.id.action_delete_all_memes){
			manageMemesFragment.HandleDeleteAllMemes();
		}
		
		return (super.onOptionsItemSelected(item));
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// If nav drawer is opened, hide all the action items
		// otherwise, show them only when it makes sense.
		boolean drawerOpen = mLateralLayout.isDrawerOpen(mLateralList);

		if(drawerOpen)
		{
			menu.findItem(R.id.action_new).setVisible(false);
			menu.findItem(R.id.action_delete_all_memes).setVisible(false);

		}
		else
		{
			menu.findItem(R.id.action_new).setVisible(m_ShowAddNewMemeAction);
			menu.findItem(R.id.action_delete_all_memes).setVisible(m_ShowDeleteAllMemes);
		}

		return super.onPrepareOptionsMenu(menu);
	}
	
	private class SlideMenuClickListener implements	ListView.OnItemClickListener {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			displayView(position);
			
		}
	}
	
	private void displayView(int position) {
		Fragment fragment = null;
		m_ShowAddNewMemeAction = true;
		m_ShowDeleteAllMemes = false;
		switch (position) {
		case IDX_LATERALMENU_HOME:
			fragment = new DiceFragment();
			break;
		case IDX_LATERALMENU_ADD_MEME:
			fragment = new AddNewMemeFragment();
			m_ShowAddNewMemeAction = false;
			break;
		case IDX_LATERALMENU_MANAGE_MEMES:
			fragment = manageMemesFragment = new ManageMemesFragment();
			m_ShowDeleteAllMemes = true;
			break;
		case IDX_LATERALMENU_SETTINGS:
			startActivity(new Intent(this, SettingsActivity.class));
			return;
		case IDX_LATERALMENU_HELP:
			fragment = new HelpFragment();
			break;

		default:
			break;
		}

		if (fragment != null) {
			FragmentManager fragmentManager = getSupportFragmentManager();
			fragmentManager.beginTransaction()
					.replace(R.id.frame_container, fragment).commit();

			// update selected item and title, then close the drawer
			mLateralList.setItemChecked(position, true);
			mLateralList.setSelection(position);
			setTitle(mMenuValues[position]);
			mLateralLayout.closeDrawer(mLateralList);
		} else { // error in creating fragment
			
		}
	}
	
	@Override
	public void setTitle(CharSequence title) {
		appName = title;
		getSupportActionBar().setTitle(appName);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mLateralToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mLateralToggle.onConfigurationChanged(newConfig);
	}
	
}