package com.example.talkativememe.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.actionbarsherlock.app.SherlockDialogFragment;
import com.example.talkativememe.R;



public class AboutDialog extends SherlockDialogFragment{
	
	private Button bOk;

    public AboutDialog() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_info_dialog, container);
        bOk = (Button) view.findViewById(R.id.info_ok);
        getDialog().setTitle("Información");
        
        bOk.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				dismiss();
			}
		});

        return view;
    }

}
