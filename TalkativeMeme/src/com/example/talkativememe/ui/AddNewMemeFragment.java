package com.example.talkativememe.ui;

import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;
import com.example.talkativememe.R;
import com.example.talkativememe.TalkativeMeme;
import com.example.talkativememe.db.MySQLiteHelper;

public class AddNewMemeFragment extends SherlockFragment {
	
	private Chronometer chronometer;
	final Handler myHandler = new Handler();
	Timer myTimer;
	
	@Override
	public void onPause() {
		super.onPause();
		handleExitFromFragment();
	}	

	@Override
	public void onStop() {
		super.onStop();
		handleExitFromFragment();
	}

	private void handleExitFromFragment() {
		if (TalkativeMeme.getInstance().getAudioRecordPlayManager().ImRecording())
			TalkativeMeme.getInstance().getAudioRecordPlayManager().stopRecording();

		if(myTimer != null) myTimer.cancel();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.activity_add_new_meme,
				container, false);

		chronometer = (Chronometer) view.findViewById(R.id.chronometer);
		setChronometerVisibility();

		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		Button addNewTextEntry = (Button) getView()
				.findViewById(R.id.buttonAddTextMeme);
		Button addNewAudioEntry = (Button) getView()
				.findViewById(R.id.buttonRecording);

		addNewTextEntry.setText(getResources().getString(
				R.string.add_new_text_meme,
				TalkativeMeme.getInstance().getMaxLengthTextEntry()));
		addNewAudioEntry.setText(getResources().getString(
				R.string.start_recording,
				TalkativeMeme.getInstance().getMaxLengthAudioSeconds()));

		addNewTextEntry.setOnClickListener(addNewMemeListener);
		addNewAudioEntry.setOnClickListener(addNewMemeListener);
	}

	private void setChronometerVisibility() {
		chronometer.setVisibility(TalkativeMeme.getInstance()
				.getAudioRecordPlayManager().ImRecording() ? View.VISIBLE
				: View.INVISIBLE);
	}
	
	private OnClickListener addNewMemeListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.buttonAddTextMeme:
				EditText pureTextEntryEditText = (EditText) getView()
						.findViewById(R.id.textMemeEditText);
				String enteredText = pureTextEntryEditText.getText().toString();

				if ("".equals(enteredText)) {
					Toast toast = Toast.makeText(getActivity(), getResources()
							.getString(R.string.error_empty_string),
							Toast.LENGTH_SHORT);

					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				} else {
					TalkativeMeme.getInstance().AddNewMeme(
							MySQLiteHelper.MEME_TYPE_TEXT, enteredText,
							enteredText);
					pureTextEntryEditText.setText("");
					Toast toast = Toast.makeText(getActivity(), getResources()
							.getString(R.string.meme_added),
							Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}
				break;
			case R.id.buttonRecording:
				Button buttonRecording = (Button) getView()
						.findViewById(R.id.buttonRecording);
				if (TalkativeMeme.getInstance().getAudioRecordPlayManager()
						.ImRecording()) {
					myTimer.cancel();
					handleRecordingStop();
				} else {
					if (!TalkativeMeme.getInstance().isSdCardMounted(false))
						return;
					InputMethodManager inputManager = (InputMethodManager) getActivity()
							.getSystemService(Context.INPUT_METHOD_SERVICE);
					inputManager.hideSoftInputFromWindow(getActivity()
							.getCurrentFocus().getWindowToken(),
							InputMethodManager.HIDE_NOT_ALWAYS);

					TalkativeMeme.getInstance().getAudioRecordPlayManager()
							.startRecording();
					buttonRecording.setText(R.string.stop_recording);
					chronometer.setBase(SystemClock.elapsedRealtime());
					chronometer.start();

					myTimer = new Timer();
					myTimer.schedule(new TimerTask() {
						@Override
						public void run() {
							UpdateGUI();
						}
					}, TalkativeMeme.getInstance()
							.getMaxLengthAudioMs());
				}
				setChronometerVisibility();
				break;
			}

		}
	};


	private void UpdateGUI() {
		myHandler.post(myRunnable);
	}

	private void handleRecordingStop() {
		Button buttonRecording = (Button) getView()
				.findViewById(R.id.buttonRecording);
		TalkativeMeme.getInstance().getAudioRecordPlayManager().stopRecording();
		buttonRecording.setText(getResources().getString(
				R.string.start_recording,
				TalkativeMeme.getInstance().getMaxLengthAudioSeconds()));
		chronometer.stop();

		TalkativeMeme.getInstance().AddNewMeme(
				MySQLiteHelper.MEME_TYPE_AUDIO,
				TalkativeMeme.getInstance().getAudioRecordPlayManager()
						.getFileNameOnly(), null);
	}
	
	final Runnable myRunnable = new Runnable() {
		public void run() {
			handleRecordingStop();
			setChronometerVisibility();
		}
	};


}
