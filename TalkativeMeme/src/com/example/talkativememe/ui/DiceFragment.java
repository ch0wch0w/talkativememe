package com.example.talkativememe.ui;

import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.actionbarsherlock.app.SherlockFragment;
import com.example.talkativememe.R;
import com.example.talkativememe.TalkativeMeme;
import com.example.talkativememe.model.MemeModel;

public class DiceFragment extends SherlockFragment implements SensorEventListener {

	public static final String MEME_TO_PLAY = "com.example.talkativememe.MEME_TO_PLAY";
	
	private boolean isInForeground = false;

	private float mLastX, mLastY, mLastZ;
	private boolean isAccelerometerInitialized;
	private SensorManager mSensorManager;
	private Sensor mAccelerometer;
	private static final int SHAKE_THRESHOLD = 600;
	private long lastUpdate;

	private boolean isDiceAnimationInProgress;
	
	MemeModel nextRandomMeme;

	final Handler myHandler = new Handler();
	Timer myTimer;
	Timer fragTransitionTimer;
	
	private GestureDetector mGestureDetector;

	// Shared Preferences
	SharedPreferences sharedPrefs;

	AnimationDrawable frameAnimation;
	String m_pathAnim1, m_pathAnim2, m_pathAnim3;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.dice_activity, container, false);

		setupGestureDetector();


		// How to detect doubletap on a View? 
		// Your view needs to implement the onTouchEvent() method, and that method 
		// needs to pass the event along to the onTouchEvent() method of the GestureDetector object.
		// Fragments don't do onTouchEvent, this is done by the view hierarchy.  
		// So your fragment generates its view hierarchy in onCreateView(), and 
		// you implement that view hierarchy to handle touch events as desired.
		view.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				return mGestureDetector.onTouchEvent(event);
			}
		});

		isDiceAnimationInProgress = false;

		return view;
	}
	
	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSensorChanged(SensorEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		m_pathAnim1 = "";
		m_pathAnim2 = "";
		m_pathAnim3 = "";

		setupAnimationDrawableForMainAnimation();

		// Set the volume control stream
		getActivity().setVolumeControlStream(AudioManager.STREAM_MUSIC);

	}
	
	@Override
	public void onResume() {
		super.onResume();

		getActivity().setTitle(
				TalkativeMeme.getInstance().getMainActivityTitle());

		isAccelerometerInitialized = false;
		mSensorManager = (SensorManager) getActivity().getSystemService(
				Context.SENSOR_SERVICE);
		mAccelerometer = mSensorManager
				.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		if (null != mAccelerometer) {
			mSensorManager.registerListener(this, mAccelerometer,
					SensorManager.SENSOR_DELAY_NORMAL);
		}

		isInForeground = true;

		TalkativeMeme.getInstance().playSelection(new MemeModel());

		setupAnimationDrawableForMainAnimation();

	}

	@Override
	public void onPause() {
		super.onPause();
		isInForeground = false;
		mSensorManager.unregisterListener(this);
		TalkativeMeme.getInstance().getAudioRecordPlayManager().stopPlaying();
	}

	private void setupGestureDetector() {
		mGestureDetector = new GestureDetector(getActivity(),
				new GestureDetector.SimpleOnGestureListener() {
					@Override
		            public boolean onDown(MotionEvent e) {
		                return true;
		            }

					@Override
					public boolean onDoubleTap(MotionEvent e) {
						if (!isDiceAnimationInProgress)
							myStartAnimation();
						return true;
					}
				});
	}
	
	@SuppressLint("NewApi")
	void setupAnimationDrawableForMainAnimation() {
		frameAnimation = createAnimationDrawable();

		ImageView img = (ImageView) getView().findViewById(
				R.id.diceShakeImageView);

		if (Build.VERSION.SDK_INT >= 16)
			img.setBackground(frameAnimation);
		else {
			img.setBackground(frameAnimation.getFrame(0));
			img.setImageDrawable(frameAnimation);
		}

	}
	
	private void myStartAnimation() {
		mSensorManager.unregisterListener(this);
		isDiceAnimationInProgress = true;

		nextRandomMeme = TalkativeMeme.getInstance().getRandomEntry();

		// Don't listen to the accelerometer input for some seconds
		myTimer = new Timer();
		myTimer.schedule(new TimerTask() {
			@Override
			public void run() {
				UpdateAccelerometer();
			}
		}, 5000);

		// The transition to PapiroActivity starts a little earlier
		fragTransitionTimer = new Timer();
		fragTransitionTimer.schedule(new TimerTask() {
			@Override
			public void run() {
				activityTransitionGo();
			}
		}, 4500);

		startAnimation();
	}
	
	private void UpdateAccelerometer() {
		myHandler.post(myAccelRunnable);
	}

	private void activityTransitionGo() {
		myHandler.post(swapFragRunnable);
	}
	
	private AnimationDrawable createAnimationDrawable() {

		AnimationDrawable newAnim = new AnimationDrawable();
		Drawable dice1, dice2, dice3;

		dice1 = new BitmapDrawable(getResources(), TalkativeMeme.getInstance()
				.getBackgroundBitmap(0));
		dice2 = new BitmapDrawable(getResources(), TalkativeMeme.getInstance()
				.getBackgroundBitmap(1));
		dice3 = new BitmapDrawable(getResources(), TalkativeMeme.getInstance()
				.getBackgroundBitmap(2));

		// TODO: The animation has fixed timings, just for simplicity.
		// Consider something fancier in the future
		newAnim.addFrame(dice1, 600);
		newAnim.addFrame(dice2, 600);
		newAnim.addFrame(dice3, 600);
		newAnim.addFrame(dice2, 500);
		newAnim.addFrame(dice3, 500);
		newAnim.addFrame(dice2, 400);
		newAnim.addFrame(dice3, 400);
		newAnim.addFrame(dice2, 300);
		newAnim.addFrame(dice3, 300);
		newAnim.addFrame(dice2, 200);
		newAnim.addFrame(dice3, 200);
		newAnim.addFrame(dice2, 100);
		newAnim.addFrame(dice3, 100);
		newAnim.addFrame(dice2, 50);
		newAnim.addFrame(dice3, 50);
		newAnim.addFrame(dice1, 50);
		newAnim.setOneShot(true);
		newAnim.setAlpha(255);

		return newAnim;
	}
	
	final Runnable swapFragRunnable = new Runnable() {
		public void run() {
			if (isInForeground) {
				Intent intent = new Intent(getActivity(), LectorActivity.class);
				intent.putExtra(MEME_TO_PLAY, nextRandomMeme);
				startActivity(intent);
			}
		}
	};

	final Runnable myAccelRunnable = new Runnable() {
		public void run() {
			if (isInForeground) {
				if (null != mAccelerometer) {
					mSensorManager.registerListener(DiceFragment.this,
							mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
				}
			}

			// Sin esto se repite animacion �?
			isAccelerometerInitialized = false;

			isDiceAnimationInProgress = false;
		}
	};
	
	public void startAnimation() {
		frameAnimation.setVisible(false, true);
		frameAnimation.stop();
		frameAnimation.selectDrawable(0);
		frameAnimation.setOneShot(true);

		frameAnimation.start();
	}

}
