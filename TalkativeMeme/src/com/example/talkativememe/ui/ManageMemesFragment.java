package com.example.talkativememe.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.database.Cursor;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.EditText;
import android.widget.FilterQueryProvider;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockListFragment;
import com.example.talkativememe.R;
import com.example.talkativememe.TalkativeMeme;
import com.example.talkativememe.adapters.ManageMemesCustomCursorAdapter;
import com.example.talkativememe.db.MemesDAO;
import com.example.talkativememe.model.MemeModel;

public class ManageMemesFragment extends SherlockListFragment {
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.activity_manage_memes, container,	false);

		new LoadCursorTask().execute();

		// Hide the progress spin
		getActivity().setProgressBarIndeterminateVisibility(false);

		// Set the volume control stream
		getActivity().setVolumeControlStream(AudioManager.STREAM_MUSIC);

		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		// Add the context menu to the listview
		ListView theList = (ListView) getView().findViewById(android.R.id.list);
		registerForContextMenu(theList);

		// not long click needed
		theList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				view.showContextMenu();
			}
		});
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		MenuInflater inflater = getActivity().getMenuInflater();
		inflater.inflate(R.menu.context_menu, menu);
	}
	
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
				.getMenuInfo();

		final MemeModel selectedMeme = 
				MemesDAO.cursorToMeme(
						((ManageMemesCustomCursorAdapter) getListView()	.getAdapter()).getCursor());

		int id= item.getItemId();
		if (id== R.id.context_menu_edit){
			
			final EditText txtInputUtente = new EditText(getActivity());
			txtInputUtente.append(selectedMeme.getUserEditedMeme());
			Resources myRes = getResources();
			new AlertDialog.Builder(getActivity())
					.setTitle(myRes.getString(R.string.title_user_edit_note))
					.setMessage(
							myRes.getString(R.string.message_user_edit_note))
					.setView(txtInputUtente)
					.setPositiveButton(myRes.getString(R.string.info_ok),
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int whichButton) {
									String inputUtente = txtInputUtente
											.getText().toString();

									if (!("").equals(inputUtente)) {
										TalkativeMeme.getInstance().updateText(
												selectedMeme.getId(),
												selectedMeme.getMemeType(),
												inputUtente);

										refreshListView();
									}
								}
							})
					.setNegativeButton(myRes.getString(R.string.cancel),
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int whichButton) {
								}
							}).show();
			return true;
		} else if (id == R.id.context_menu_delete ) {
			if (TalkativeMeme.getInstance().isSdCardMounted(false)) {
				int rowsDeleted = TalkativeMeme.getInstance().deleteMeme(
						selectedMeme.getId());

				if (rowsDeleted > 0) {
					refreshListView();
					resetSearchFilter();
				}
			}
			return true;
		} else if (id==R.id.context_menu_play){

			if (TalkativeMeme.getInstance().isSdCardMounted(false)) {
				TalkativeMeme.getInstance().playSelection(selectedMeme);
			}

			return true;
		} else {
			return super.onContextItemSelected(item);
		}
	}
	
	@Override
	public void onResume() {
		TalkativeMeme.getInstance().playSelection(new MemeModel());

		refreshListView();
		super.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
		TalkativeMeme.getInstance().getAudioRecordPlayManager().stopPlaying();

	}
	
	public void HandleDeleteAllMemes() {
		// Exit immediately if the sd card is not mounted
		if (!TalkativeMeme.getInstance().isSdCardMounted(false))
			return;

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

		// set title
		alertDialogBuilder.setTitle(getResources().getString(
				R.string.alert_delete_all_confirm));

		// set dialog message
		alertDialogBuilder.setMessage(getResources().getString(R.string.alert_delete_all_title));
		alertDialogBuilder.setCancelable(false);
		alertDialogBuilder.setPositiveButton(android.R.string.yes,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, close
						// current activity
						int rowsDeleted = TalkativeMeme.getInstance().deleteAllMemes();

						if (rowsDeleted > 0) {
							refreshListView();
							resetSearchFilter();
						}
					}
				});
		alertDialogBuilder.setNegativeButton(android.R.string.no,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}
	
	private void refreshListView() {
		new LoadCursorTask().execute();
	}

	void resetSearchFilter() {
		EditText filterText = (EditText) getView().findViewById(R.id.searchFilter);
		filterText.setText("");
	}
	private class LoadCursorTask extends AsyncTask<Void, Void, Void> {
		private Cursor memesCursor = null;

		@Override
		protected Void doInBackground(Void... params) {
			memesCursor = TalkativeMeme.getInstance().getAllMemes();
			memesCursor.getCount();

			return (null);
		}

		@Override
		public void onPostExecute(Void arg0) {
			final ManageMemesCustomCursorAdapter adapter;

			adapter = new ManageMemesCustomCursorAdapter(getActivity()
					.getApplicationContext(), memesCursor, 0);

			setListAdapter(adapter);

			// Search EditText
			EditText myFilter = (EditText) getView().findViewById(R.id.searchFilter);
			myFilter.addTextChangedListener(new TextWatcher() {
				
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					adapter.getFilter().filter(s.toString());
					
				}
				
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count,	int after) {
					
				}
				
				@Override
				public void afterTextChanged(Editable s) {
					
				}
			});

			adapter.setFilterQueryProvider(new FilterQueryProvider() {
				
				@Override
				public Cursor runQuery(CharSequence constraint) {
					return TalkativeMeme.getInstance().getEntriesByName(constraint.toString());
				}
			});

		}
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();

		((ManageMemesCustomCursorAdapter) getListAdapter()).getCursor().close();
	}

}
