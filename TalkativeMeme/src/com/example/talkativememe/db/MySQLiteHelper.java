package com.example.talkativememe.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MySQLiteHelper extends SQLiteOpenHelper {

	public static final String DATABASE_NAME = "allMemes.db";
	public static final int DATABASE_VERSION = 2;
	
	public static final String TABLE_NAME = "Memes";
	
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_MEME_TYPE = "memeType";
	public static final String COLUMN_ACTUAL_MEME = "actualMeme";
	public static final String COLUMN_USER_EDITED_MEME = "userEditedMeme";
	public static final String COLUMN_FILE_PATH = "filePath";
	
	public static final long MEME_TYPE_TEXT = 1;
	public static final long MEME_TYPE_AUDIO = 2;
	public static final long MEME_TYPE_PHOTO = 3;
	public static final long MEME_TYPE_VIDEO = 4;
	public static final long MEME_TYPE_OTHER = 99;

	private static final String DATABASE_CREATE = "create table "
			+ TABLE_NAME + "(" 
			+ COLUMN_ID	+ " integer primary key autoincrement, " 
			+ COLUMN_MEME_TYPE + " integer not null, "
			+ COLUMN_ACTUAL_MEME + " text not null, "
			+ COLUMN_USER_EDITED_MEME + " text not null"
			+ ");";
	
	private Context context;

	public MySQLiteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		
		this.context=context;
		
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(DATABASE_CREATE);		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
		onCreate(db);
		
	}

}
