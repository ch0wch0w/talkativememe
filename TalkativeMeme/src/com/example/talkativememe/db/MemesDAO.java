package com.example.talkativememe.db;

import java.io.File;

import com.example.talkativememe.TalkativeMeme;
import com.example.talkativememe.model.MemeModel;
import com.example.talkativememe.R;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class MemesDAO {

	private SQLiteDatabase database;
	private MySQLiteHelper dbHelper;
	private Context context;
	
	private String[] allColumns = { MySQLiteHelper.COLUMN_ID,
			MySQLiteHelper.COLUMN_MEME_TYPE,
			MySQLiteHelper.COLUMN_ACTUAL_MEME,
			MySQLiteHelper.COLUMN_USER_EDITED_MEME };
	
	public MemesDAO(Context context) {
		this.context = context;
		dbHelper = new MySQLiteHelper(context);
	}
	
	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}
	
	public void openForReading() throws SQLException {
		database = dbHelper.getReadableDatabase();
	}

	public void close() {
		dbHelper.close();
	}
	
	public void createMeme(long memeType, String actualMeme,
			String userEditedMeme) {
		ContentValues values = new ContentValues();

		// String userEditedMeme = actualMeme;
		int intMemeType = (int) memeType;

		// If a user edited meme is not specified, it defaults to the kind of file
		// or to the initial string set by the user
		if (userEditedMeme == null) {
			switch (intMemeType) {
			case (int) MySQLiteHelper.MEME_TYPE_AUDIO:
				userEditedMeme = "AUDIO";
				break;

			case (int) MySQLiteHelper.MEME_TYPE_PHOTO:
				userEditedMeme = "PHOTO";
				break;

			case (int) MySQLiteHelper.MEME_TYPE_VIDEO:
				userEditedMeme = "VIDEO";
				break;

			default:
				userEditedMeme = actualMeme;
				break;
			}
		}

		values.put(MySQLiteHelper.COLUMN_MEME_TYPE, memeType);
		values.put(MySQLiteHelper.COLUMN_ACTUAL_MEME, actualMeme);
		values.put(MySQLiteHelper.COLUMN_USER_EDITED_MEME, userEditedMeme);

		database.insert(MySQLiteHelper.TABLE_NAME, null,
				values);
	}
	
	public int deleteMeme(long id) {
		Cursor cursor = null;
		boolean deleted = false;
		int rowsDeleted = 0;

		try {

			cursor = database.rawQuery("SELECT * FROM "
					+ MySQLiteHelper.TABLE_NAME + " WHERE "
					+ MySQLiteHelper.COLUMN_ID + "=?",
					new String[] { String.valueOf(id) });

			// meme with -> delete file first
			if (cursor.getCount() > 0) {
				cursor.moveToFirst();
				long entryType = cursor.getLong(cursor
						.getColumnIndex(MySQLiteHelper.COLUMN_MEME_TYPE));
				String fileName = null;

				if (isMemeNotText(entryType)) {
					fileName = cursor
							.getString(cursor
									.getColumnIndex(MySQLiteHelper.COLUMN_ACTUAL_MEME));
					File file = new File(TalkativeMeme.getInstance()
							.getAppFilesPath() + File.separator + fileName);
					deleted = file.delete();
				} else
					deleted = true;
			}
		} finally {
			cursor.close();
		}

		// delete meme from the database
		if (deleted) {
			rowsDeleted = database.delete(MySQLiteHelper.TABLE_NAME,
					MySQLiteHelper.COLUMN_ID + " = " + id, null);
		}

		return rowsDeleted;
	}
	
	public int updateText(long id, long memeType, String newText) {
		ContentValues values = new ContentValues();
		values.put(MySQLiteHelper.COLUMN_USER_EDITED_MEME, newText);

		// If text-only meme, also update the actual meme
		if(memeType == MySQLiteHelper.MEME_TYPE_TEXT)
			values.put(MySQLiteHelper.COLUMN_ACTUAL_MEME, newText);

		return database.update(MySQLiteHelper.TABLE_NAME, values,
				MySQLiteHelper.COLUMN_ID + " = ?", new String[] { String.valueOf(id) });
	}
	
	public int deleteAllMemes() {	
		int rowsDeleted = 0;

		// Delete all text-only memes 
		rowsDeleted = database.delete(MySQLiteHelper.TABLE_NAME,
				MySQLiteHelper.COLUMN_MEME_TYPE + " = '"
						+ MySQLiteHelper.MEME_TYPE_TEXT + "'", null);

		// Delete all files
		Cursor cursor = null;
		boolean deleted = false;
		try {

			cursor = database.rawQuery("SELECT * FROM "
					+ MySQLiteHelper.TABLE_NAME, null);

			cursor.moveToFirst();

			while (!cursor.isAfterLast()) {

				long entryType = cursor.getLong(cursor
						.getColumnIndex(MySQLiteHelper.COLUMN_MEME_TYPE));
				String fileName = null;

				if (isMemeNotText(entryType)) {
					fileName = cursor
							.getString(cursor
									.getColumnIndex(MySQLiteHelper.COLUMN_ACTUAL_MEME));
					File file = new File(TalkativeMeme.getInstance()
							.getAppFilesPath() + File.separator + fileName);
					deleted = file.delete();
					cursor.moveToNext();
				} else
					deleted = true;
			}
		} finally {
			cursor.close();
		}

		// COMPROBAR ESTO
		rowsDeleted = deleted ? 0 : rowsDeleted + database.delete(MySQLiteHelper.TABLE_NAME, null, null);

		return rowsDeleted;
	}

	public Cursor getAllMemes() {
		return database.query(MySQLiteHelper.TABLE_NAME,
				allColumns, null, null, null, null, MySQLiteHelper.COLUMN_USER_EDITED_MEME + " Collate NOCASE");
	}
	
	public Cursor getMemesByName(String inputText) throws SQLException {
		Cursor mCursor = null;
		if (inputText == null || inputText.length() == 0) {
			mCursor = database.query(MySQLiteHelper.TABLE_NAME,
					allColumns, null, null, null, null, MySQLiteHelper.COLUMN_USER_EDITED_MEME + " Collate NOCASE");

		} else {
			mCursor = database.query(true, MySQLiteHelper.TABLE_NAME, allColumns, 
					MySQLiteHelper.COLUMN_USER_EDITED_MEME + " like '" + inputText + "%'", null, null, null, null,
					null);
		}
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;

	}
	
	public MemeModel getRandomMeme() {
		MemeModel entry = new MemeModel();

		// Initializes meme with the message shown when there are no memes stored
		Resources myRes = context.getResources();
		String warning = myRes.getString(R.string.no_memes_warning);
		entry.setActualMeme(warning);
		entry.setUserEditedMeme(warning);
		entry.setMemeType(MySQLiteHelper.MEME_TYPE_TEXT);
		entry.setId(-1);

		Cursor cursor = database.query(MySQLiteHelper.TABLE_NAME,
				allColumns, null, null, null, null, null);

		// Random choice
		if (cursor.getCount() > 0) {
			cursor.moveToPosition((int) (Math.random() * cursor.getCount()));
			entry = cursorToMeme(cursor);
		}

		cursor.close();

		return entry;
	}
	
	private boolean isMemeNotText(long entryType) {
		return (entryType == MySQLiteHelper.MEME_TYPE_AUDIO)
				|| (entryType == MySQLiteHelper.MEME_TYPE_PHOTO)
				|| (entryType == MySQLiteHelper.MEME_TYPE_VIDEO);
	}

	public static MemeModel cursorToMeme(Cursor cursor) {
		MemeModel meme = new MemeModel();
		meme.setId(cursor.getLong(0));
		meme.setMemeType(cursor.getLong(1));
		meme.setActualMeme(cursor.getString(2));
		meme.setUserEditedMeme(cursor.getString(3));
		return meme;
	}
	
}
