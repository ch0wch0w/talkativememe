package com.example.talkativememe.service;

import java.util.Locale;

import com.example.talkativememe.R;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.widget.Toast;

public class VozService extends Service implements OnInitListener {

	private TextToSpeech tts;
	private boolean isTtsInitialized;
	public static String MEME_TO_SAY = "com.example.talkativememe.MEME_TO_SAY";
	private String meme;
	
	@Override
	public void onCreate() {
		super.onCreate();

		isTtsInitialized = false;
		tts = new TextToSpeech(this, this);
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		meme = intent.getStringExtra(MEME_TO_SAY);

		if(tts == null) {
			isTtsInitialized = false;
			tts = new TextToSpeech(this, this);

			if(!("".equals(meme)))
				Toast.makeText(getApplicationContext(),
					getResources().getString(R.string.tts_init_in_progress),
					Toast.LENGTH_SHORT).show();
		}
		else
		{
			if(!("".equals(meme)))
				sayIt(meme);
		}
		//Log.d(TAG, "onStartCommand quote=" + quote);

		return START_NOT_STICKY;
	}
	
	private void sayIt(String texto) {
		if(!isTtsInitialized) {
			Toast.makeText(getApplicationContext(),
					getResources().getString(R.string.tts_init_in_progress),
					Toast.LENGTH_SHORT).show();
			return;
		}

		if (tts != null) {
			if (texto != null) {
				if (!tts.isSpeaking()) {
					tts.speak(texto, TextToSpeech.QUEUE_FLUSH, null);
				}
			}
		}

		
	}

	@Override
	public void onDestroy() {
		shutdownTTS();
	}
	
	@Override
	public void onInit(int value) {
		if (value == TextToSpeech.SUCCESS) {
			Locale current = getResources().getConfiguration().locale;
			tts.setLanguage(current);

			switch (tts.isLanguageAvailable(current)) {
			case TextToSpeech.LANG_AVAILABLE:
			case TextToSpeech.LANG_COUNTRY_AVAILABLE:
			case TextToSpeech.LANG_COUNTRY_VAR_AVAILABLE:

				tts.setLanguage(current);
				isTtsInitialized = true;

				if(!("".equals(meme)))
					Toast.makeText(this,
							getResources().getString(R.string.tts_init_done),
							Toast.LENGTH_SHORT).show();
				break;
			case TextToSpeech.LANG_MISSING_DATA:

				Intent installIntent = new Intent();
				installIntent
						.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
				installIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				isTtsInitialized = true;
				startActivity(installIntent);			

				break;
			case TextToSpeech.LANG_NOT_SUPPORTED:
				Locale spanish = new Locale("es", "ES");
				tts.setLanguage(spanish);
				break;
			}

		} else {
			tts = null;
			Toast.makeText(this,
					getResources().getString(R.string.tts_init_failed),
					Toast.LENGTH_SHORT).show();
		}
		
	}

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void shutdownTTS() {
		if (tts != null) {
			tts.stop();
			tts.shutdown();
		}
	}

}
