package com.example.talkativememe.model;

import com.example.talkativememe.db.MySQLiteHelper;

import android.os.Parcel;
import android.os.Parcelable;

public class MemeModel implements Parcelable {

	private long id;
	private String actualMeme;
	private long memeType;
	private String userEditedMeme;
	// private String filePath;
	// TODO implement files: images, audio, video
	
	

	public String getUserEditedMeme() {
		return userEditedMeme;
	}

	public void setUserEditedMeme(String userEditedMeme) {
		this.userEditedMeme = userEditedMeme;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getActualMeme() {
		return actualMeme;
	}

	public void setActualMeme(String actualMeme) {
		this.actualMeme = actualMeme;
	}
	
	public long getMemeType() {
		return memeType;
	}

	public void setMemeType(long memeType) {
		this.memeType = memeType;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel parcel, int arg1) {
		parcel.writeLong(id);
		parcel.writeString(actualMeme);
		parcel.writeLong(memeType);
		parcel.writeString(userEditedMeme);

		//parcel.writeValue(filePath);
		
	}
	
	public static final Parcelable.Creator<MemeModel> CREATOR = new Parcelable.Creator<MemeModel>() {
		public MemeModel createFromParcel(Parcel in) {
			return new MemeModel(in);
		}

		public MemeModel[] newArray(int size) {
			return new MemeModel[size];
		}
	};
	
	private MemeModel(Parcel in) {
		this.id = in.readLong();
		this.actualMeme = in.readString();
		this.memeType = in.readLong();
		this.userEditedMeme = in.readString();

		//this.filePath = in.readString(); 
	}
	
	public MemeModel() {
		id = 0;
		actualMeme = "";
		memeType = MySQLiteHelper.MEME_TYPE_TEXT;
		userEditedMeme = "";
	}

	@Override
	public String toString() {
		return userEditedMeme;
	}

}
