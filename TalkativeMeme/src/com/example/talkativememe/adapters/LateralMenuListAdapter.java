package com.example.talkativememe.adapters;

import java.util.ArrayList;

import com.example.talkativememe.R;
import com.example.talkativememe.model.LateralMenuItem;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class LateralMenuListAdapter extends BaseAdapter {

	 private Context context;
	 private ArrayList<LateralMenuItem> mItems;
	
	 public LateralMenuListAdapter(Context context, ArrayList<LateralMenuItem> mItems){
	        this.context = context;
	        this.mItems = mItems;
	    }
	 
	@Override
	public int getCount() {
		return mItems.size();
	}

	@Override
	public Object getItem(int pos) {
		return mItems.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int pos, View view, ViewGroup parent) {
		if (view == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            view = mInflater.inflate(R.layout.lateral_menu_item, null);
        }
          
        ImageView imgIcon = (ImageView) view.findViewById(R.id.icon);
        TextView txtTitle = (TextView) view.findViewById(R.id.title);
          
        imgIcon.setImageResource(mItems.get(pos).getIcon());        
        txtTitle.setText(mItems.get(pos).getName());
         
        return view;
	}

}
