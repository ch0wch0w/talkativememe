package com.example.talkativememe.adapters;

import com.example.talkativememe.R;
import com.example.talkativememe.TalkativeMeme;
import com.example.talkativememe.db.MemesDAO;
import com.example.talkativememe.db.MySQLiteHelper;
import com.example.talkativememe.model.MemeModel;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class ManageMemesCustomCursorAdapter extends CursorAdapter {

	private LayoutInflater mInflater;

	public ManageMemesCustomCursorAdapter(Context context, Cursor c, int flags) {
		super(context, c, flags);
		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		final MemeModel meme = MemesDAO.cursorToMeme(cursor);

		TextView txtDesc = (TextView) view.findViewById(R.id.desc);

		ImageView imageView = (ImageView) view.findViewById(R.id.icon);

		txtDesc.setText(meme.getUserEditedMeme());

		int iconId = R.drawable.ic_action_edit;

		if (meme.getMemeType() == MySQLiteHelper.MEME_TYPE_AUDIO)
			iconId = R.drawable.ic_action_microphone;

		//TODO implementar!
		if (meme.getMemeType() == MySQLiteHelper.MEME_TYPE_PHOTO)
			iconId = R.drawable.ic_action_photo;
		if (meme.getMemeType() == MySQLiteHelper.MEME_TYPE_VIDEO)
			iconId = R.drawable.ic_action_video;

		imageView.setImageResource(iconId);
		imageView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(TalkativeMeme.getInstance().isSdCardMounted(false)) {
					TalkativeMeme.getInstance().playSelection(meme);
				}
			}
		});
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		
		return mInflater.inflate(R.layout.manage_memes_list_row, parent,
				false);
	}

}
